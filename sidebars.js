module.exports = {
  someSidebar: {
    Welcome: [
      'intro'
    ],
    ["How it Works"]: [
      'email',
      'deviceSyncing',
      'backups'
    ],
    Security: [
      'accountCreation',
      'encryption'
    ],
    FAQ: [
      'faqGeneral',
      'faqPrivacy',
      'faqEncryption'
    ],
  },
};
